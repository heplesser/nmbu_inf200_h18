# INF200 Advanced Programming

Welcome to INF200 *Advanced Programming* at NMBU in the fall and winter term 2018/19. This Bitbucket repository will provide source code and other material used in the course.

The
[Wiki for the course](https://bitbucket.org/heplesser/nmbu_inf200_h18/wiki)
will be the main forum for all matters pertaining to the course. The
Canvas page for the course will not be used except to indicate
approved exercises.

Enjoy this course, and remember that Python rocks!


For demonstration purposes, I add another sentence to the README.
And in the second lab session, I add one more line.


