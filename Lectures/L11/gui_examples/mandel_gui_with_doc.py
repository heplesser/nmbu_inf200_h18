# -*- coding: utf-8 -*-

"""
GUI for exploring Mandelbrot sets.

Note:
    Before running this code from within PyCharm, you need to go to
    Settings > Tools > Python Scientific and remove the checkmark for
    "Show plots in tool window". Otherwise, PyCharm will display figures
    in the right sidebar without possibility for interaction.

References and sources:
    https://matplotlib.org/gallery/widgets/slider_demo.html
    https://matplotlib.org/users/event_handling.html
    https://matplotlib.org/api/widgets_api.html
    https://matplotlib.org/examples/widgets/buttons.html

For more ideas, see
    https://www.ibm.com/developerworks/community/blogs/jfp/entry/My_Christmas_Gift?lang=en
    https://www.ibm.com/developerworks/community/blogs/jfp/entry/How_To_Compute_Mandelbrodt_Set_Quickly?lang=en
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


import numba
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cmaps
import matplotlib.colors as colors
from matplotlib.widgets import RectangleSelector, Button, Slider, RadioButtons


class MandelModel:
    """
    Computes Mandelbrot Set.
    """

    DEFAULT_ITERATIONS = 100
    DEFAULT_SIZE = 1000

    def __init__(self, nx=DEFAULT_SIZE, ny=DEFAULT_SIZE):
        """
        :param nx: Number of data points in x-direction
        :param ny: Number of data points in y-direction
        """
        self._nx = nx
        self._ny = ny
        self.reset()

    def reset(self):
        """
        Set model data members to initial state
        """
        self._x = np.linspace(-2, 1, num=self._nx)
        self._y = np.linspace(-1.5, 1.5, num=self._ny)

        self._iterations = self.DEFAULT_ITERATIONS

        # Data is expensive to compute. Therefore, create empty NumPy array
        # of correct size and mark it as not valid
        self._data = np.empty((self._nx, self._ny))
        self._data_valid = False

    @property
    def x(self):
        '1D array with x-axis coordinates.'
        return self._x

    @property
    def y(self):
        '1D array with y-axis coordinates.'
        return self._y

    @property
    def data(self):
        '2D array with number of iterations before escape.'
        if not self._data_valid:
            # Recompute data if required
            self._mandel_driver()

        # At this point, _data is valid.
        return self._data

    @property
    def iterations(self):
        'Maximum number of iterations to perform.'
        return self._iterations

    @iterations.setter
    def iterations(self, value):
        if value < 1:
            raise ValueError('Number of iterations >= 1 required.')
        self._iterations = int(value)

        # Changing the iteration limit invalidates stored data
        self._data_valid = False

    def set_limits(self, xmin, xmax, ymin, ymax):
        """
        Set limits for range to evaluate.

        :param xmin: minimal x value
        :param xmax: maximal x value
        :param ymin: minimal y value
        :param ymax: maximal y value
        """
        if xmin >= xmax:
            raise ValueError('xmin < xmax required.')
        if ymin >= ymax:
            raise ValueError('ymin < ymax required.')

        self._x = np.linspace(xmin, xmax, num=self._nx)
        self._y = np.linspace(ymin, ymax, num=self._ny)

        # Changing the coordinate ranges invalidates stored data
        self._data_valid = False

    @staticmethod
    @numba.jit
    def _mandel_iter(x, y, max_iter):
        """
        Evaluate single data point.

        Note: A staticmethod is part of a class, but does not receive self.

        :param x: x coordinate
        :param y: y coordinate
        :param max_iter: maximal number of iterations to perform
        :return: number of iterations required for escape, or max_iter
        """
        c = complex(x, y)
        z = 0.0j
        for n in range(max_iter):
            z = z * z + c
            if abs(z) > 2:
                return n + 1
        return max_iter

    @numba.jit
    def _mandel_driver(self):
        """
        Compute Mandelbrot set for defined x and y ranges.
        """
        for i, y in enumerate(self.y):
            for j, x in enumerate(self.x):
                self._data[i, j] = self._mandel_iter(x, y, self._iterations)
        self._data_valid = True


class MandelGui:
    """
    GUI displaying Mandelbrot set, allowing user to zoom in.
    """

    # Selection of colormaps from
    # https://matplotlib.org/gallery/color/colormap_reference.html
    COLORMAPS =['viridis', 'inferno', 'inferno_r', 'hot',
                'seismic', 'ocean', 'jet', 'jet_r']
    DEFAULT_COLORMAP = 'inferno'

    def __init__(self, model):
        """
        :param model: Model instance with interface of MandelGUI
        """

        self._model = model

        # ---------------------------------------------

        # Create a figure, which will hold the entire GUI
        self._fig = plt.figure()

        # Axes for plotting Mandelbrot set
        self._ax_plot = self._fig.add_axes([0.1, 0.15, 0.7, 0.8])

        # Axes for iterations slider and colormap radio buttons
        self._ax_iters = self._fig.add_axes([0.15, 0.03, 0.3, 0.03])
        self._ax_cmap = self._fig.add_axes([0.82, 0.4, 0.15, 0.5])

        # Axes for reset and quit buttons
        self._ax_reset = self._fig.add_axes([0.82, 0.15, 0.15, 0.05])
        self._ax_quit = self._fig.add_axes([0.82, 0.05, 0.15, 0.05])

        # ---------------------------------------------

        # Add widgets to the axes. We need to assign the widgets
        # to member variables so they are not deleted by garbage collection.

        # Widget allowing user to select zoom area in the plot
        self._w_rect = RectangleSelector(self._ax_plot,
                                         self._select_zoom,  # method to call
                                         useblit=True,  # ensure rectangle shows
                                         drawtype='box')

        # ..............................................

        # Widget for the slider, also define initial parameter value
        self._w_iters = Slider(self._ax_iters,
                               label='Iterations',        # label for slider
                               valmin=0, valmax=1000,     # min, max value
                               valinit=model.iterations,  # initial value
                               valstep=1,                 # stepping for value
                               valfmt='%.0f')        # format for value display
        # Tell slider widget what to do when user changes value
        self._w_iters.on_changed(self._set_iters)

        # ..............................................

        # Widget for radio buttons to select colormap
        self._cmap = self.DEFAULT_COLORMAP
        self._w_cmap = RadioButtons(self._ax_cmap,
                                    labels=self.COLORMAPS)

        # Activate button for initial colormap
        self._w_cmap.set_active(self.COLORMAPS.index(self._cmap))

        # Tell radio button widget what to do when user makes selection
        self._w_cmap.on_clicked(self._set_cmap)

        # ..............................................

        # Widget for reset button, and action to perform
        self._w_reset = Button(self._ax_reset, 'Reset')
        self._w_reset.on_clicked(self._reset)

        # ..............................................

        # Widget for quit button, and action to perform
        self._w_quit = Button(self._ax_quit, 'Quit')
        self._w_quit.on_clicked(self._quit)

    def show(self):
        """
        Displays the GUI and starts infinite loop.

        To exit, the user needs to push the Quit button.
        """

        self._update()   # make sure plot is up-to-date
        plt.show()

    def _select_zoom(self, ev_click, ev_release):
        """
        Respond to rectangular selection by setting model limits.

        :param ev_click: Event describing mouse-button press
        :param ev_release: Event describing mouse-button release
        """

        self._model.set_limits(min(ev_click.xdata, ev_release.xdata),
                               max(ev_click.xdata, ev_release.xdata),
                               min(ev_click.ydata, ev_release.ydata),
                               max(ev_click.ydata, ev_release.ydata))
        self._update()

    def _set_iters(self, value):
        """
        Set number of iterations to new value.
        """

        self._model.iterations = value
        self._update()

    def _set_cmap(self, label):
        """
        Select new colormap.
        """

        self._cmap = label
        self._update()

    def _reset(self, _):
        """
        Reset GUI and model.
        """

        # Model handles number of iteration and coordinate ranges
        self._model.reset()

        # GUI handles colormap
        self._cmap = self.DEFAULT_COLORMAP

        # Need to reset slider and radio buttons
        self._w_iters.set_val(self._model.iterations)
        self._w_cmap.set_active(self.COLORMAPS.index(self._cmap))

        # Finally, update the figure
        self._update()

    def _quit(self, _):
        """
        Close figure and exit
        """

        plt.close(self._fig)

    def _update(self):
        """
        Update graphic according to current parameter value.

        :return:
        """

        self._ax_plot.clear()
        self._ax_plot.pcolormesh(self._model.x,
                                 self._model.y,
                                 self._model.data,
                                 cmap=cmaps.get_cmap(self._cmap),
                                 norm=colors.LogNorm())
        self._ax_plot.set_aspect('equal')
        self._fig.canvas.draw_idle()


if __name__ == '__main__':
    mmodel = MandelModel()
    MandelGui(mmodel).show()
