# -*- coding: utf-8 -*-

"""
Example solution to EX06 Task A.
"""

import re

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def select_int(filename):
    """
    Returns list with all lines containing "int".
    """

    with open(filename, 'r') as text:
        return [line.strip()
                for line in text
                if re.search('int', line)]


def select_at(filename):
    """
    Returns list with all lines containing the word "at".
    """

    with open(filename, 'r') as text:
        return [line.strip()
                for line in text
                if re.search(r'\b[Aa]t\b', line)]


def upper_cat(filename):
    """
    Returns list with all lines, "cat" replaced by "CAT".
    """

    with open(filename, 'r') as text:
        return [re.sub('cat', 'CAT', line).strip()
                for line in text]
