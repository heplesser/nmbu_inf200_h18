# -*- coding: utf-8 -*-

import pytest

"""
Tests for median function.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def median(data):
    """
    Returns median of data.

    :param data: An iterable of containing numbers
    :return: Median of data
    """

    if len(data) == 0:
        raise ValueError
    
    s_data = sorted(data)
    n = len(s_data)
    return (s_data[n // 2] if n % 2 == 1
            else 0.5 * (s_data[n // 2 - 1] + s_data[n // 2]))


def test_empty():
    """Does median raise ValueError on empty list?"""

    with pytest.raises(ValueError):
        median([])

    with pytest.raises(ValueError):
        median(())


def test_single():
    """Correct result for single-element list?"""
    assert median([5]) == 5


def test_odd_num_elements():
    """Test median for lists with odd number of elements."""
    assert median([3, 1, 2]) == 2
    assert median([3, 1, 2, 3, 5]) == 3


def test_even_num_elements():
    """Test median for lists with even number of elements."""
    assert median([3, 1, 2, 4]) == 2.5
    assert median([3, 2, 1, 4, 3, 5]) == 3


def test_sorted_elements():
    """Test median for list with sorted elements."""
    assert median([1, 2, 3]) == 2
    assert median([1, 2, 3, 4]) == 2.5


def test_reverse_elements():
    """Test median for list with reverse-sorted elements."""
    assert median([3, 2, 1]) == 2
    assert median([4, 3, 2, 1]) == 2.5


def test_argument_unchanged():
    """Test that argument passed to median remains unchanged."""
    data = [4, 5, 1, 3, 2]
    median(data)
    assert data == [4, 5, 1, 3, 2]


def test_tuple_argument():
    """Test that median function works for tuples, too."""
    assert median((1, 2, 3)) == 2
    assert median((1, 2, 3, 4)) == 2.5
