# -*- coding: utf-8 -*-

"""
Exercise 02-A: Counting character frequencies in a file.

Part of a course at Norges miljø- og biovitenskapelige universitet på Ås.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def char_counts(text_filename):
    counts = [0] * 256
    with open(text_filename, encoding='utf-8') as textfile:
        for char in textfile.read():
            counts[ord(char)] += 1
    return counts


if __name__ == '__main__':

    filename = 'file_stats.py'
    frequencies = char_counts(filename)
    for code in range(256):
        if frequencies[code] > 0:
            print('{:3}{:>4}{:6}'.format(code,
                                         chr(code) if code >= 32 else '',
                                         frequencies[code]))
