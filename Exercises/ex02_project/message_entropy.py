# -*- coding: utf-8 -*-

"""
Exercise 02-B: Message entropy
"""

from math import log2

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def letter_freq(string):
    lc_string = string.lower()
    return {char: lc_string.count(char) for char in set(lc_string)}


def entropy(message):

    if not message:
        return 0

    length = len(message)
    frequencies = letter_freq(message)
    return -sum(count/length * log2(count/length)
                for count in frequencies.values()
                if count > 0)

if __name__ == "__main__":
    for msg in '', 'aaaa', 'aaba', 'abcd', 'This is a short text.':
        print('{:25}: {:8.3f} bits'.format(msg, entropy(msg)))
