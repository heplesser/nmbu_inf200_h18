# -*- coding: utf-8 -*-

"""

"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def bubble_sort(in_data):
    s_data = list(in_data)
    for j in reversed(range(len(s_data))):
        for k in range(j):
            if s_data[k+1] < s_data[k]:
                s_data[k], s_data[k+1] = s_data[k+1], s_data[k]
    return s_data


if __name__ == "__main__":

    for data in ((),
                 (1,),
                 (1, 3, 8, 12),
                 (12, 8, 3, 1),
                 (8, 3, 12, 1)):
        print('{!s:>15} --> {!s:>15}'.format(data, bubble_sort(data)))

    from random import random, randint, seed
    seed(12334467)
    for _ in range(10):
        d = [random() for _ in range(randint(10, 1000))]
        if bubble_sort(d) == sorted(d):
            print("Passed")
        else:
            print("Error sorting {}".format(d))
