# -*- coding: utf-8 -*-

"""
Random walk simulation

This program simulates a random symmetric 1D random walk
from a starting point to a target.
"""

import random

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Walker:
    """
    A random walker.

    A walker starts at a given position and has a home.
    """

    def __init__(self, start, home):
        """
        :param start: initial position of the walker
        :param home: position of the walker's home
        """
        self.x = start
        self.home = home
        self.steps = 0

    def get_position(self):
        """Returns current position."""

        return self.x

    def get_steps(self):
        """Returns steps taken so far."""

        return self.steps

    def is_at_home(self):
        """Returns True if walker is at home position."""

        return self.x == self.home

    def move(self):
        """
        Change coordinate by +1 or -1 with equal probability.
        """

        self.x += 2 * random.randint(0, 1) - 1
        self.steps += 1


def steps_to_home(start, home):
    """
    Returns number of steps required to reach home from start.
    """

    walker = Walker(start, home)

    while not walker.is_at_home():
        walker.move()

    return walker.get_steps()


if __name__ == '__main__':

    distances = [1, 2, 5, 10, 20, 50, 100]
    num_trials = 5
    seed = 1

    random.seed(seed)

    for dist in distances:
        path_lengths = [steps_to_home(0, dist) for _ in range(num_trials)]
        print('Distance: {:3d} -> Path lengths: {}'.format(
                                                    dist, sorted(path_lengths)))
