# -*- coding: utf-8 -*-

"""
Program illustrating polymorphism by way of random number generator classes.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class ListRand:
    """
    Random number generator providing numbers from a list.

    :note:
    - These numbers will be returned as random numbers, so they should come
      from a good source of randomness.
    - If more numbers are requested than are in the list, RuntimeError
      is raised.
    """

    def __init__(self, numbers):
        """
        :param numbers: list of random numbers to use
        """

        self._random_numbers = numbers.copy()
        self._next = 0

    def rand(self):
        """
        Returns next random number.
        """

        if self._next >= len(self._random_numbers):
            raise RuntimeError('Random number list exhausted.')

        r = self._random_numbers[self._next]
        self._next += 1
        return r


class LCGRand:
    """
    Linear congruential random number generator.

    Numbers are obtained from r_{n+1} = a r_m mod m
    with a = 7**5 and m = 2**31-1 as suggested by
    Park & Miller (Comm ACM 31:1192-1201, 1988).

    This generator is reasonable, but the sequence
    length too short by today's standards.
    """

    a = 7 ** 5
    m = 2 ** 31 - 1

    def __init__(self, seed):
        """
        :param seed: random number generator seed
        """

        self._previous = seed

    def rand(self):
        """
        Return next random number.
        """

        self._previous = (LCGRand.a * self._previous) % LCGRand.m
        return self._previous


if __name__ == '__main__':

    lg1 = ListRand([45, 2, 34, 1, 8, 12])
    lcg1 = LCGRand(346)

    print("Drawing random numbers")
    fmt = "{:>10}   {:>10}"
    print(fmt.format("ListRNG", "LCG"))
    for _ in range(5):
        print(fmt.format(lg1.rand(), lcg1.rand()))
