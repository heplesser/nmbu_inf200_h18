# -*- coding: utf-8 -*-

"""
Minimal set of tests for EX07 Task C.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

from gambler import propagator, init_state, evolve, t_win, t_lose, plot_analysis

import pytest
import numpy as np
import matplotlib.pyplot as plt


class TestGambler:
    """
    Tests for EX07 Task C.
    """

    def test_propagator(self):
        T = propagator(10, 0.25)
        assert T.shape == (11, 11)
        assert T[0, 0] == 1
        assert T[10, 10] == 1
        assert all(T.diagonal()[1:-1] == 0)
        assert all(T.diagonal(1)[:-1] == 1-0.25)
        assert all(T.diagonal(-1)[1:] == 0.25)
        assert all(T.sum(axis=0) == 1)

    def test_init_state(self):
        q0 = init_state(10, 3)
        assert q0.shape == (11, 1) or q0.shape == (11,)
        assert q0[3] == 1
        assert all(q0[:3] == 0) and all(q0[4:] == 0)

    def test_evolve(self):
        T = propagator(10, 0.25)
        q0 = init_state(10, 3)
        Q = evolve(T, q0, 100)
        assert Q.shape == (11, 101)
        assert (Q[:, 0] == q0).all()
        assert (Q >= 0).all()

    def test_t_win(self):
        T = propagator(10, 0.25)
        q0 = init_state(10, 3)
        Q = evolve(T, q0, 100)
        tw = t_win(Q)
        assert tw.shape == (100,) or tw.shape == (1, 100)
        assert tw == pytest.approx(np.diff(Q[10, :]))

    def test_t_lose(self):
        T = propagator(10, 0.25)
        q0 = init_state(10, 3)
        Q = evolve(T, q0, 100)
        tl = t_lose(Q)
        assert tl.shape == (100,) or tl.shape == (1, 100)
        assert tl == pytest.approx(np.diff(Q[0, :]))

    def test_plot_analysis(self):
        fig = plot_analysis(10, 3, 0.25, 100)
        assert isinstance(fig, plt.Figure)
