# -*- coding: utf-8 -*-

"""
Minimal set of tests for EX07 Task B.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

from weather import load_data, plot_temperature

import matplotlib.pyplot as plt


class TestWeather:
    """
    Tests for EX07 Task B.
    """

    def test_load_data(self):
        d = load_data('Aas dogn 2003-2012.xlsx', 2004, 2007)
        assert all(d.columns == ['DATE', 'TAM', 'TAN', 'TAX', 'RR',
                                 'RRM', 'GLOB', 'UV', 'TG'])
        assert d.shape == (366+3*365, 9)

    def test_plot_temp(self):
        d = load_data('Aas dogn 2003-2012.xlsx', 2003, 2012)
        fig = plot_temperature(d)
        assert isinstance(fig, plt.Figure)
