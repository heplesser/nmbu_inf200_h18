# -*- coding: utf-8 -*-

"""
Minimal set of tests for EX07 Task A.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

from pendulum import dydt, psim, plot_psim

import numpy as np
import matplotlib.pyplot as plt


class TestPendulum:
    """
    Tests for EX07 Task A.
    """

    def test_dydt(self):
        y0_orig = np.array([0.1, 0.2, 0, 0])
        y0 = y0_orig.copy()
        dy = dydt(y0, 0, 1, 2)
        assert dy.shape == y0.shape
        assert all(y0 == y0_orig)

    def test_psim(self):
        dt = 1
        t_max = 10
        res = psim(0.1, 0.2, 1, 2, dt, t_max)
        assert res.shape == (t_max//dt + 1, 5)

    def test_plot_psim(self):
        fig = plot_psim(0.1, 0.2, 1, 2, 1, 10)
        assert isinstance(fig, plt.Figure)
