# -*- coding: utf-8 -*-

"""
Random walk simulation within boundaries.

This program simulates a random symmetric 1D random walk
from a starting point to a target.
"""

from walker_sim import Walker, Simulation

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class BoundedWalker(Walker):
    """
    A random walker in a bounded environment.

    A walker starts at a given position and has a home.
    She is limited to a certain environment.
    """

    def __init__(self, start, home, left_limit, right_limit):
        """
        :param start: initial position of the walker
        :param home: position of the walker's home
        :param left_limit: left boundary of walker movement
        :param right_limit: right boundary of walker movement
        """

        if not (left_limit <= start <= right_limit):
            raise ValueError('Starting position must be within boundaries.')
        if not (left_limit <= home <= right_limit):
            raise ValueError('Home position must be within boundaries.')

        super().__init__(start, home)
        self.left = left_limit
        self.right = right_limit

    def move(self):
        """
        Change coordinate by +1 or -1 with equal probability, respecting limits.
        """

        super().move()
        if self.position < self.left:
            self.position = self.left
        elif self.position > self.right:
            self.position = self.right


class BoundedSimulation(Simulation):
    """
    Implements a complete random walker simulation.
    """

    def __init__(self, start, home, seed, left_limit, right_limit):
        """
        :param start: walker's initial position
        :param home: walk ends when walker reaches home
        :param seed: random generator seed
        :param left_limit: left boundary of walker movement
        :param right_limit: right boundary of walker movement
        """

        super().__init__(start, home, seed)
        self.left = left_limit
        self.right = right_limit

    def single_walk(self):
        """
        Simulate single walk from start to home, returning number of steps.

        :returns: number of steps taken
        """

        walker = BoundedWalker(self.start, self.home,
                               self.left, self.right)
        num_steps = 0
        while not walker.is_at_home():
            walker.move()
            num_steps += 1
        return num_steps


if __name__ == '__main__':
    for left_bound in [0, -10, -100, -1000, -10000]:
        steps = BoundedSimulation(0, 20, seed=12345,
                                  left_limit=left_bound,
                                  right_limit=20).run_simulation(20)
        print('Left boundary: {:8d}: {}'.format(left_bound, steps))
