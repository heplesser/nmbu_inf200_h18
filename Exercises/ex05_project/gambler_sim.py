# -*- coding: utf-8 -*-

"""
Gambler's ruin simulation.
"""

import random

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Gambler:
    """
    A gambler.

    The gambler starts with a certain amount of money and continues until she
    is broke or has broken the bank.
    """

    def __init__(self, initial, total, p=0.5):
        """
        :param initial: initial amount the player owns
        :param total: total amount of money in the game
        :param p: probability of winning
        """

        if not (0 <= initial <= total):
            raise ValueError('Inconsistent initial and total amount.')
        if not (0 <= p <= 1):
            raise ValueError('0 <= p <= 1 required.')

        self._m = initial
        self._M = total
        self._p = p

    def is_broke(self):
        """Returns True if player is broke."""

        return self._m <= 0

    def owns_all(self):
        """Returns True if player owns everything."""

        return self._m >= self._M

    def play(self):
        """
        Flip coin and update wealth accordingly.
        """

        if random.random() < self._p:
            self._m += 1
        else:
            self._m -= 1


class GamblerSimulation:
    """
    Implements a complete gambler simulation.
    """

    def __init__(self, initial, total, p, seed):
        """
        :param initial: initial amount the player owns
        :param total: total amount of money in the bank
        :param p: probability of winning
        :param seed: random generator seed
        """

        self._initial = initial
        self._total = total
        self._p = p

        random.seed(seed)

    def single_game(self):
        """
        Simulate single game.

        :returns: ruin, duration
        """

        gambler = Gambler(self._initial, self._total, self._p)
        num_steps = 0
        while not (gambler.is_broke() or gambler.owns_all()):
            gambler.play()
            num_steps += 1
        return gambler.is_broke(), num_steps

    def run_simulation(self, num_games):
        """
        Run a set of games.

        :param num_games: number of games to simulate
        :returns: list of wins and list of losses, each with durations
        """

        results = [self.single_game() for _ in range(num_games)]
        return ([dur for ruin, dur in results if not ruin],
                [dur for ruin, dur in results if ruin])


if __name__ == '__main__':
    for prob in [0.0, 0.1, 0.2, 0.4, 0.45, 0.49, 0.5, 0.9]:
        wins, ruins = GamblerSimulation(initial=25,
                                        total=100,
                                        p=prob,
                                        seed=12345).run_simulation(20)
        print('p: {:5.2f}, wins: {:3d}, losses: {:3d}'.format(prob, len(wins),
                                                              len(ruins)))
        print('    Win durations :', wins)
        print('    Loss durations:', ruins)
        print()
