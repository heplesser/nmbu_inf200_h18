# -*- coding: utf-8 -*-

"""
Random walk simulation

This program simulates a random symmetric 1D random walk
from a starting point to a target.
"""

import random

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


class Walker:
    """
    A random walker.

    A walker starts at a given position and has a home.
    """

    def __init__(self, start, home):
        """
        :param start: initial position of the walker
        :param home: position of the walker's home
        """
        self.position = start
        self.home = home
        self._steps = 0

    def get_position(self):
        """Returns current position."""

        return self.position

    def get_steps(self):
        """Returns number of steps taken by walker."""

        return self._steps

    def is_at_home(self):
        """Returns True if walker is at home position."""

        return self.position == self.home

    def move(self):
        """
        Change coordinate by +1 or -1 with equal probability.
        """

        self.position += 2 * random.randint(0, 1) - 1
        self._steps += 1


class Simulation:
    """
    Implements a complete random walker simulation.
    """

    def __init__(self, start, home, seed):
        """
        :param start: walker's initial position
        :param home: walk ends when walker reaches home
        :param seed: random generator seed
        """

        self.start = start
        self.home = home
        random.seed(seed)

    def single_walk(self):
        """
        Simulate single walk from start to home, returning number of steps.

        :returns: number of steps taken
        """

        walker = Walker(self.start, self.home)
        while not walker.is_at_home():
            walker.move()
        return walker.get_steps()

    def run_simulation(self, num_walks):
        """
        Run a set of walks, returns list of number of steps taken.

        :param num_walks: number of walks to simulate
        :returns: list with number of steps per walk
        """

        return [self.single_walk() for _ in range(num_walks)]


if __name__ == '__main__':
    print(Simulation(0, 10, 12345).run_simulation(20))
    print(Simulation(0, 10, 12345).run_simulation(20))
    print(Simulation(0, 10, 54321).run_simulation(20))

    print(Simulation(10, 0, 12345).run_simulation(20))
    print(Simulation(10, 0, 12345).run_simulation(20))
    print(Simulation(10, 0, 54321).run_simulation(20))
